package section03.backend;


import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class CharacterCounterTests {

    @Test(expected = IllegalArgumentException.class)
    public void testNullInput(){
        CharacterCounter.countCharacters(null);
    }
    @Test
    public void testStringInput(){
        Map<Character, Integer> map = CharacterCounter.countCharacters("!a!A!");
        assertEquals(map.size(), 3);
        assertEquals(map.get('a').intValue(), 1);
        assertEquals(map.get('!').intValue(), 3);
        assertEquals(map.get('A').intValue(), 1);
    }

}
